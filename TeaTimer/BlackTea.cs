﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeaTimer
{
    class BlackTea : Tea
    {
        public BlackTea() : base("Black", 208, 5.0)
        {

        }
    }
}
