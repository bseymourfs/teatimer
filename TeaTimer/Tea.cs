﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace TeaTimer
{
    class Tea
    {
        public string Name { get; private set; }
        public int WaterTemp { get; private set; }
        public double SteepTime { get; private set; }

        private int Seconds = 0;

        public Tea(string name, int temp, double steepTime)
        {
            this.Name = name;
            this.WaterTemp = temp;
            this.SteepTime = steepTime;
        }

        public string ShortDescription()
        {
            string description = $"{Name} Tea ({WaterTemp}°F)";
            return description;
        }

        public void StartTimer()
        {
            Console.Clear();
            // create a new timer that fires every second
            Timer timer = new Timer(1000);
            timer.Elapsed += new ElapsedEventHandler(HandleTimer);
            timer.Start();
            // call handler since .Start() doesn't call it at the beginning.
            HandleTimer(timer, null);
            Console.Read();
        }

        private void HandleTimer(object sender, EventArgs e)
        {
            Seconds++;
            double secondsLeft = (this.SteepTime * 60) - Seconds;
            
            Console.Clear();
            // Cancel
            if (secondsLeft < 1.0)
            {
                Timer timer = (Timer)sender;
                timer.Stop();

                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine("Timer Done!");
                Console.BackgroundColor = ConsoleColor.Black;

                // Beep 5 times
                for (int i = 0; i < 5; i++)
                {
                    Console.Beep();
                }
                Seconds = 0;
                return;
            }


            string minutesLeft = SecondsToMinutes(secondsLeft);
            Console.WriteLine($"*** {this.Name} Tea ***");
            Console.WriteLine("Time remaining: "+minutesLeft);
        }

        private string SecondsToMinutes(double seconds)
        {
            TimeSpan time = TimeSpan.FromSeconds(seconds);
            // Add a zero if need: ex. 09 vs 9
            string secondsString = time.Seconds < 10 ? ("0" + time.Seconds.ToString()) : time.Seconds.ToString();
            string minutes = $"{time.Minutes}:{secondsString}";
            return minutes;
        }
    }
}
