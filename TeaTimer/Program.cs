﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace TeaTimer
{
    class Program
    {
        static Menu mainMenu;

        static void Main(string[] args)
        {
            GreenTea greenTea = new GreenTea();
            BlackTea blackTea = new BlackTea();
            OolongTea oolongTea = new OolongTea();

            string[] items = new string[] { greenTea.ShortDescription(), blackTea.ShortDescription(), oolongTea.ShortDescription(), };

            List<Action> actions = new List<Action> {
                () => TeaSelected(greenTea),
                () => TeaSelected(blackTea),
                () => TeaSelected(oolongTea)
            };

            // instantiate and show a new Menu with actions linked
            mainMenu = new Menu(items, actions);
            mainMenu.Show("Select a tea: ");
            
        }

        public static void TeaSelected(Tea tea)
        {
            string yOrN = Validator.GetString($"Would you like to brew some {tea.Name} Tea? [y/n]", "Please enter Y or N.", (string input) => {
                if (input.ToLower() == "y" || input.ToLower() == "n") return true;
                return false;
            });

            if (yOrN.ToLower() == "n")
            {
                // reshow menu
                mainMenu.Show("Select a tea:");
            }
            else
            {
                tea.StartTimer();
            }
        }
    }
}
