﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeaTimer
{
    class GreenTea : Tea
    {
        public GreenTea() : base("Green", 175, 3.0)
        {
            
        }
    }
}
